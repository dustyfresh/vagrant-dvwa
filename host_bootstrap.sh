#!/usr/bin/env bash
# script to bootstrap DVWA env with vagrant

# pull repo
git clone git@bitbucket.org:dustyfresh/vagrant-dvwa.git

# buld environment
cd vagrant-dvwa && vagrant box add trusty-server-cloudimg-amd64-vagrant-disk1.box --name ubuntu/trusty64
vagrant box list
printf "we are ready to rock! Start the bridged vagrant environment with the following command:\ncd vagrant-dvwa && vagrant up\n"
