## Damn Vulnerable Web App Vagrant environment
[Damn Vulnerable Web App](https://github.com/RandomStorm/DVWA) vagrant things. Should make bootstrapping a new instance for testing much easier..

### requirements
- vagrant w/ Virtualbox guest additions
- ubuntu14.04 vagrant box

```
$ curl -s https://bitbucket.org/dustyfresh/vagrant-dvwa/raw/910b3a7c5e74f789caacdd0753ef2751b3ac1542/host_bootstrap.sh | bash
```
